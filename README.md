# Project setup
-----------
Make sure you have installed docker, and docker-compose on your machine.

Clone the project:
`git clone https://shpetim_selaci@bitbucket.org/softuptechnologies/apollofederation-react-cypress-typescript.git`

To setup && run project:
`docker-compose up --build`

To just run the project:d
`docker-compose up --d`

If you want to see all the logs:
`docker-compose logs -f`